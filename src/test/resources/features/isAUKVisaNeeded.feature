# new feature
# Tags: optional

@visaTests
Feature: UK visa requirement form, test scenarios

  Background:
    Given I open the UK visa check site
    And I click start now

  Scenario: Validate visa requirements for persona A
    Given I provide a nationality of 'Japan'
    And I click next step
    And I select the reason 'Study'
    And I click next step
    And I state I am intending to stay for 'longer than six months'
    When I submit the form
    Then Then I will be informed 'You’ll need a visa to study in the UK'

  Scenario: Validate visa requirements for persona B
    Given I provide a nationality of 'Japan'
    And I click next step
    And I select the reason 'Tourism'
    When I submit the form
    Then Then I will be informed 'You won’t need a visa to come to the UK'

  Scenario: Validate visa requirements for persona C
    Given I provide a nationality of 'Russia'
    And I click next step
    And I select the reason 'Tourism'
    And I click next step
    And I state No, I am not travelling with or visiting a partner or family
    When I submit the form
    Then Then I will be informed 'You’ll need a visa to come to the UK'