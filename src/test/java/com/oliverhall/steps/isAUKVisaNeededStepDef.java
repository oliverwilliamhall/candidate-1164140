package com.oliverhall.steps;

import io.cucumber.java.en.Given;
import com.oliverhall.pageobjects.*;


public class isAUKVisaNeededStepDef {

    private GovCheckUKVisa govCheckUKVisa;
    private YourNationalityPage yourNationalityPage;
    private ReasonForTravelPage reasonForTravelPage;
    private DurationOfStayPage durationOfStayPage;
    private OutcomeSummaryPage outcomeSummaryPage;
    private TourismExtendedQuestionPage tourismExtendedQuestionPage;

    @Given("^I open the UK visa check site$")
    public void i_open_the_first_page() {
    govCheckUKVisa.openPage();
    }

    @Given("^I click start now$")
    public void i_click_start_now() {
        govCheckUKVisa.iCLickStartNow();
    }

    @Given("^I click next step$")
    public void i_click_next_step() {
        govCheckUKVisa.iClickNextStep();
    }

    @Given("^I select the reason '(.*)'$")
    public void i_select_the_reason(String reasonForTravel) {
        reasonForTravelPage.iSelectTheRadioButton(reasonForTravel);
    }

    @Given("^I provide a nationality of '(.*)'$")
    public void i_provide_a_nationality_of(String nationality) {
        yourNationalityPage.iExpandYourNationalityDropdown();
        yourNationalityPage.iSelectFromDropDown(nationality);
    }

    @Given("^I state I am intending to stay for '(.*)'$")
    public void i_state_i_am_intending_to_stay_for(String durationOfStay) {
        durationOfStayPage.iSelectTheRadioButton(durationOfStay);
    }

    @Given("^I state No, I am not travelling with or visiting a partner or family")
    public void i_state_i_am_not_with_family_or_friends() {
        tourismExtendedQuestionPage.iSelectTheRadioButton("no");
    }

    @Given("^I submit the form")
    public void i_select_the_reason() {
        govCheckUKVisa.iClickNextStep();
    }

    @Given("^Then I will be informed '(.*)'$")
    public void i_will_be_informed(String expectedMessage) {
        outcomeSummaryPage.assertOutcomeStatement(expectedMessage);
    }

}
