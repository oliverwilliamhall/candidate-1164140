package com.oliverhall.steps;

        import io.cucumber.java.en.Given;
        import com.oliverhall.apiservice.*;


public class postCodeQueryStepDef {

    private PostCodeIO postCodeIO; ;

    @Given("^I send a get request to postcodes io$")
    public void i_get_postcode_io() {
        postCodeIO.configureBaseUrl();
    }

    @Given("^I get a '(.*)' response code$")
    public void i_get_this_response(Integer expectedCode) {
        postCodeIO.assertExpectedResponseCode(expectedCode);
    }


}

