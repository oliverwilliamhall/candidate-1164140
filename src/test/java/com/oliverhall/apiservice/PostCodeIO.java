package com.oliverhall.apiservice;

import io.restassured.response.Response;
import io.restassured.RestAssured;
import net.serenitybdd.core.pages.PageObject;
import org.junit.Assert;

public class PostCodeIO extends PageObject {

    private String baseUrl = "http://api.postcodes.io";
    private String service = "/postcodes/";
    private String postcode = "SW1P4JA";
    private Response resp;

    public void configureBaseUrl() {
        resp=RestAssured.get(baseUrl+service+postcode);
    }

    public void assertExpectedResponseCode(Integer expectedCode){
        Integer responseCode = resp.getStatusCode();
        Assert.assertEquals("Postcode IO did not return the expected response code",expectedCode,responseCode);
    }

}
