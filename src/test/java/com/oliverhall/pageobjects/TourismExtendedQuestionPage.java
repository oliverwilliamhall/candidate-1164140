package com.oliverhall.pageobjects;

        import net.serenitybdd.core.annotations.findby.FindBy;
        import net.serenitybdd.core.pages.PageObject;
        import net.serenitybdd.core.pages.WebElementFacade;
        import org.openqa.selenium.By;
        import java.util.List;

public class TourismExtendedQuestionPage extends PageObject {

    @FindBy(className = "govuk-radios")
    WebElementFacade radioButtonGroup;

    @FindBy(className = "gem-c-radio govuk-radios__item")
    List<WebElementFacade> listOfPossibleRadioButtons;

    public void iSelectTheRadioButton(String option){
        getDriver().findElement(By.xpath("//div[@class='govuk-radios']//input[@value='"+option.toLowerCase()+"']")).click();
    }

}


