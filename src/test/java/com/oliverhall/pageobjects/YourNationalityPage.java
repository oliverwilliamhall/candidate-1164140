package com.oliverhall.pageobjects;

        import net.serenitybdd.core.annotations.findby.FindBy;
        import net.serenitybdd.core.pages.PageObject;
        import net.serenitybdd.core.pages.WebElementFacade;
        import org.openqa.selenium.By;

public class YourNationalityPage extends PageObject {

    @FindBy(xpath = "//select[@id='response']")
    WebElementFacade nationalityDropDown;

    public void iExpandYourNationalityDropdown(){
        nationalityDropDown.click();
    }

    public void iSelectFromDropDown(String option){
        getDriver().findElement(By.xpath("//select//option [@value='"+option.toLowerCase()+"']")).click();
    }


}


