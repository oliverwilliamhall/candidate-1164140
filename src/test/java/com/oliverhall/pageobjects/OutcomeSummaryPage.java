package com.oliverhall.pageobjects;

        import net.serenitybdd.core.annotations.findby.FindBy;
        import net.serenitybdd.core.pages.PageObject;
        import net.serenitybdd.core.pages.WebElementFacade;
        import org.junit.Assert;

public class OutcomeSummaryPage extends PageObject {

    @FindBy(xpath = "//div[@id='result-info']/div//h2")
    WebElementFacade outcomeStatement;

    public void assertOutcomeStatement(String expectedOutcome){
        Assert.assertEquals("The visa outcome message is not as expected",expectedOutcome,outcomeStatement.getText());
    }


}


