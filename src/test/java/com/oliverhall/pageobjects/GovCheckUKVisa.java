package com.oliverhall.pageobjects;

        import net.serenitybdd.core.annotations.findby.FindBy;
        import net.serenitybdd.core.pages.PageObject;
        import net.serenitybdd.core.pages.WebElementFacade;
        import java.util.concurrent.TimeUnit;

public class GovCheckUKVisa extends PageObject {

    @FindBy(partialLinkText = "Start n")
    WebElementFacade startNowButton;

    @FindBy(xpath = "//button[@class='gem-c-button govuk-button gem-c-button--bottom-margin']")
    WebElementFacade nextStepButton;

    public void openPage() {
        getDriver().get("https://www.gov.uk/check-uk-visa");
    }

    public void iCLickStartNow(){
        startNowButton.click();
    }

    public void iClickNextStep(){
        nextStepButton.click();
    }

}

